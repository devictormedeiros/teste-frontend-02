## Blog Teste SEOX

Desenvolvi um site na plataforma WordPress do zero, com um tema customizado que conta com listagem de posts e páginas internas para proporcionar uma experiência agradável para o usuário. O projeto foi cuidadosamente pensado para ser otimizado para os motores de busca, utilizando diversas técnicas de SEO para garantir a melhor visibilidade online possível. Além disso, implementei estratégias de performance para que o site carregue rapidamente, proporcionando uma melhor experiência para o usuário e contribuindo para o sucesso do projeto.

Link -> https://devictormedeiros.com/teste

## SEO

- As imagens foram otimizadas para garantir um carregamento mais rápido da página.
- Foi feito o adiamento de requisições de arquivos/bibliotecas para melhorar o tempo de carregamento da página.
- Foram utilizadas tags HTML semânticas para melhorar a acessibilidade e a indexação pelos motores de busca.
- As metas do <head> foram revisadas e otimizadas para garantir um melhor desempenho nas buscas.


## Tecnologias utilizadas

- WordPress
- PHP
- HTML
- CSS/SASS
- JavaScript

## Instalação

Clonar o repositório: git clone https://gitlab.com/devictormedeiros/teste-frontend-02.git
Criar um banco de dados para hospedar o wordpress
Instalar o wordpress atualizado (6.1.1)
Criar os posts para exibir no site.


## Como contribuir

- Fork do repositório
- Criação de uma branch: git checkout -b nome-da-branch
- Commit das mudanças: git commit -am 'descrição das mudanças'
- Push para a branch: git push origin nome-da-branch
- Criar um Pull Request

## Autores
Victor Augusto Medeiros da Silva
- Site: https://devictormedeiros.com
- Linkedin: https://www.linkedin.com/in/victor-medeiros-69b825b0


## Comentários adicionais

Antes de mais nada, gostaria de agradecer a oportunidade de realizar este teste para a SEOX. Este projeto foi um ótimo desafio, espero poder integrar a equipe e continuar aprendendo cada vez mais para entregar um produto final ainda mais performático e de qualidade.

Durante o desenvolvimento, identifiquei algumas ideias que poderiam melhorar ainda mais a interação e experiência do usuário:

- Implementar um menu fixo ao rolar a página, para que o usuário possa acessá-lo a qualquer momento;
- Adicionar um botão flutuante "scroll-to-top" para que o usuário possa voltar ao topo da página com mais facilidade;
- Incluir um breadcrumbs no topo do conteúdo da listagem dos posts e na interna para ajudar o usuário a navegar de forma mais rápida;
- Adicionar um rodapé com informações de contato, ou até mesmo seguir com o menu proposto no topo da página;
- Implementar uma paginação na lista do post ou um "load more" ajax para trazer mais posts;
- Incluir um botão "ver mais" em cada card de post para incentivar ainda mais o usuário a clicar e acessar o conteúdo completo;
- Adicionar o nome do autor junto à data do post para dar mais credibilidade ao conteúdo;
- Incluir uma caixa de comentários ao final do post para estimular a interação dos usuários e o engajamento com o conteúdo;
- Implementar um formulário de newsletter para captar emails e manter os usuários informados sobre as novidades do blog.
- Acessibilidade: Opção de aumento/diminuição de fonte e contraste da página

Acredito que essas sugestões podem trazer ainda mais valor para o projeto e para o usuário final. 
Obrigado pela oportunidade novamente!