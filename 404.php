<?php get_header(); ?>

<section class="container py-md-5 py-4">
    <div class="row">
        <div class="col-lg-8 mx-auto text-center">
            <img class="d-block mx-auto" src="<?php echo get_template_directory_uri() . '/assets/img/computer.webp'; ?>" alt="">
            <h1 class="mb-3">Página não encontrada</h1>
            <p>Desculpe, a página que você está procurando não foi encontrada. Por favor, verifique se o endereço está correto ou volte para a página inicial.</p>
        </div>
    </div>
</section>

<?php get_footer(); ?>
</div>