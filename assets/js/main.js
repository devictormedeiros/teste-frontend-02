jQuery(document).ready(function ($) {

	// FUNÇÃO PARA FAZER ANCORAGEM COM EFEITO E DESCONTAR A ALTURA DO MENU FIXO 
	jQuery('a[href^="#"]').click(function (e) {
		e.preventDefault();
		var id = $(this).attr('href'),
			targetOffset = $(id).offset().top;
		$('html, body').animate({
			scrollTop: targetOffset - 90
		}, 1000);
	});

	if (window.matchMedia("(min-width: 768px)").matches) {
		if ($('.publi-container-single, .publi-container').length > 0) {
			var h_menu = $('.publi-container-single, .publi-container').offset().top;
			$(window).scroll(function () {
				if ($(window).scrollTop() > h_menu) {
					$('.publi-container-single, .publi-container').css('position', 'sticky');
					$('.publi-container-single, .publi-container').css('top', '5px');
				} else {
					$('.publi-container-single, .publi-container').css('position', 'relative');
					$('.publi-container-single, .publi-container').css('top', 'initial');
				}
			});
		}
	}

});
  // FIM DO DOCUMENT READY