<?php get_header(); ?>

<main id="main" class="site-main" role="main">
    <section class="cabecalho-archive">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-12 position-relative">
                    <?php include 'includes/cabecalho-archive.php'; ?>
                </div>
            </div>
        </div>
    </section>
    <section>
        <div class="container">
            <div class="row">
                <div class="col-lg-9 pe-lg-5">
                    <?php if (have_posts()) {
                        while (have_posts()) {
                            the_post();
                            include 'includes/post-item.php';
                        }
                    }
                    ?>
                </div>
                <div class="col-lg-3">
                    <aside class="publi-container">
                        <p><?php _e('Publicidade', 'seox-theme'); ?></p>
                        <div class="publi-card">

                        </div>
                    </aside>
                </div>
            </div>
        </div>
    </section>
</main>
<?php get_footer(); ?>