<?php
// obter a ID da imagem em destaque
$post_thumbnail_id = get_post_thumbnail_id();

// obter a URL da imagem de baixa resolução
$thumbnail_url = wp_get_attachment_image_src($post_thumbnail_id, 'thumbnail')[0];
$thumbnail_base64 = base64_encode(file_get_contents($thumbnail_url));
// obter o tamanho real da imagem
$size = getimagesize($thumbnail_url);
$width = $size[0];
$height = $size[1];
?>

<!-- exibir a imagem usando a técnica de carregamento progressivo -->

<article class="post-item">
    <?php if (get_the_post_thumbnail()) : ?>
        <div class="thumb">
            <a href="<?php the_permalink(); ?>"><img src="<?php echo get_the_post_thumbnail_url($post->ID, 'full'); ?>" data-src="data:image/jpeg;base64,<?php echo $thumbnail_base64; ?>" alt="<?php echo get_post_meta($post_thumbnail_id, '_wp_attachment_image_alt', true); ?>" width="<?php echo $width; ?>" height="<?php echo $height; ?>"></a>
        </div>
    <?php endif; ?>
    <div class="text">
        <a href="<?php the_permalink(); ?>">
            <h2><?php the_title() ?></h2>
        </a>
        <?php include 'data-info.php'; ?>
    </div>
</article>