<div class="data-info">
    <?php echo get_svg('clock'); ?>
    <p><?php echo get_the_date('d/m/Y'); ?> <time><?php echo get_the_time(); ?></time></p>
</div>