<?php
if (!is_front_page() && !is_home()) {
  $term = get_queried_object(); // Obter o objeto atual da taxonomia
  $titulo = $term->name;
} else {
  $titulo = 'Blog SEOX';
}
if (is_category()) {
  $link = get_category_link(get_queried_object_id());
} elseif (is_home() || is_front_page()) {
  $link = get_home_url();
} else {
  $link = get_permalink();
}
?>
<div class="cabecalho">
  <div class="title-cabecalho">
    <h1><?php echo $titulo; ?></h1>
  </div>
  <div class="social-top">
    <a href="https://api.whatsapp.com/send?text=<?php echo $link; ?>" target="_blank" title="Compartilhar no whatsapp" class="btn-whatsapp-icon"><?= get_svg('whatsapp-icon'); ?>compartilhar</a>
    <a href="https://t.me/share/url?url=<?php echo $link; ?>" target="_blank" title="Compartilhar no Telegram" class="btn-social-icon telegram-icon"><?= get_svg('telegram-icon'); ?></a>
    <a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $link; ?>" target="_blank" title="Compartilhar no Facebook" class="btn-social-icon facebook-icon"><?= get_svg('facebook-icon'); ?></a>
    <a href="https://twitter.com/intent/tweet?url=<?php echo $link; ?>" target="_blank" title="Compartilhar no Twitter" class="btn-social-icon twitter-icon"><?= get_svg('twitter-icon'); ?></a>
  </div>
</div>