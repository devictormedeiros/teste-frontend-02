<?php
require_once('includes/wp-bootstrap-navwalker.php');

add_theme_support('automatic-feed-links');
add_theme_support('title-tag');
add_theme_support('post-thumbnails');
add_theme_support('align-wide');
add_theme_support('menus');


if (!function_exists('_wp_render_title_tag')) {
    function renderizar_title()
    {
?>
        <title><?php wp_title('|', true, 'right'); ?></title>
<?php
    }
    add_action('wp_head', 'renderizar_title');
}


add_action('wp_enqueue_scripts', 'gf_enqueue_scripts');

function gf_enqueue_scripts()
{
    wp_enqueue_script('jquery-js', 'https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js', array(), '1.0.0', true, 'defer');
    wp_enqueue_script('bootstrap-js', 'https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js', array(), '1.0.0', true, 'defer');
    wp_enqueue_script('main-js', get_stylesheet_directory_uri() . '/assets/js/main.js', array(), '1.0.0', true, 'defer');

    wp_enqueue_style('bootstrap', 'https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css', array(), '1.0', 'all', true);
    wp_enqueue_style('main-css', get_stylesheet_directory_uri() . '/assets/css/main.css', array(), '1.0', 'all', true);
    wp_enqueue_style('responsive', get_stylesheet_directory_uri() . '/assets/css/responsive.css', array(), '1.0', 'all', true);
    wp_enqueue_style('style', get_stylesheet_directory_uri() . '/style.css', array(), '1.0', 'all', true);
}

// DEIXEI AS FUNCÇÕES DE CRIAÇÃO DE POST E TAXONOMIA SÓ PARA MOSTRAR CONHECIMENTO, MAS VI QUE NÃO PRECISA NO TESTE 

// // POSTS TYPE
// function meus_posts_types()
// {
//     register_post_type(
//         'publicacoes',
//         array(
//             'labels' => array(
//                 'name' => _('Publicações'),
//                 'singular_name' => _('Publicação')
//             ),
//             'public' => true,
//             'has_archive' => true,
//             'menu_icon' => 'dashicons-admin-tools',
//             'supports' => array('title', 'thumbnail', 'editor', 'page-attributes', 'excerpt', 'page-attributes'),
//         )
//     );
// }
// add_action('init', 'meus_posts_types', 0);

// // CRIAR TAXONOMIA
// function create_taxonomy_type()
// {
//     register_taxonomy(
//         'cat-publicacoes',
//         'publicacoes',
//         array(
//             'label' => 'Categorias de Publicações ',
//             'hierarchical' => true,
//             'show_in_nav_menus' => true,
//             'show_in_menu' => true
//         )
//     );
// }
// add_action('init', 'create_taxonomy_type');


// PEGA A THUMB DO VIDEO
function get_youtube_thumb($url)
{
    if (strpos($url, 'youtu.be') !== false) {
        $aux = explode('/', $url);
        $id = array_pop($aux);
    } else {
        parse_str(parse_url($url, PHP_URL_QUERY), $my_array_of_vars);
        $id = $my_array_of_vars['v'];
    }

    $thumb_url = 'https://img.youtube.com/vi/' . $id . '/hqdefault.jpg';

    return $thumb_url;
}


// LIMPAR NUMERO TELEFONE
function limpar_numero($str)
{
    return preg_replace("/[^0-9]/", "", $str);
}



// FUNÇÃO EMBED YOUTUBE C/ URL DO NAVEGADOR
function get_youtube_embed_url($url)
{
    if (strpos($url, 'youtu.be') !== false) {
        $aux = explode('/', $url);
        $id = array_pop($aux);
    } else {
        parse_str(parse_url($url, PHP_URL_QUERY), $my_array_of_vars);
        $id = $my_array_of_vars['v'];
    }
    $embed_url = 'http://www.youtube.com/embed/' . $id . '?autoplay=1&mute=1&enablejsapi=1';
    return $embed_url;
}

// Função get SVG
function get_svg($svg)
{
    $file = file_get_contents(get_bloginfo('template_directory') . '/assets/svg/' . $svg . '.svg');
    return $file;
}


function my_myme_types($mime_types)
{
    $mime_types['svg'] = 'image/svg+xml'; //Adding svg extension
    return $mime_types;
}
add_filter('upload_mimes', 'my_myme_types', 1, 1);


// FUNÇÃO PARA PAGINAÇÃO
function wpbeginner_numeric_posts_nav()
{

    if (is_singular())
        return;

    global $wp_query;

    /** Stop execution if there's only 1 page */
    if ($wp_query->max_num_pages <= 1)
        return;

    $paged = get_query_var('paged') ? absint(get_query_var('paged')) : 1;
    $max   = intval($wp_query->max_num_pages);

    /** Add current page to the array */
    if ($paged >= 1)
        $links[] = $paged;

    /** Add the pages around the current page to the array */
    if ($paged >= 3) {
        $links[] = $paged - 1;
        $links[] = $paged - 2;
    }

    if (($paged + 2) <= $max) {
        $links[] = $paged + 2;
        $links[] = $paged + 1;
    }

    echo '<div class="navigation"><ul>' . "\n";

    /** Previous Post Link */
    if (get_previous_posts_link())
        printf('<li class="prev-post">%s</li>' . "\n", get_previous_posts_link('<i class="fas fa-chevron-left"></i>'));

    /** Link to first page, plus ellipses if necessary */
    if (!in_array(1, $links)) {
        $class = 1 == $paged ? ' class="active"' : '';

        printf('<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url(get_pagenum_link(1)), '1');

        if (!in_array(2, $links))
            echo '<li>…</li>';
    }

    /** Link to current page, plus 2 pages in either direction if necessary */
    sort($links);
    foreach ((array) $links as $link) {
        $class = $paged == $link ? ' class="active"' : '';
        printf('<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url(get_pagenum_link($link)), $link);
    }

    /** Link to last page, plus ellipses if necessary */
    if (!in_array($max, $links)) {
        if (!in_array($max - 1, $links))
            echo '<li>…</li>' . "\n";

        $class = $paged == $max ? ' class="active"' : '';
        printf('<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url(get_pagenum_link($max)), $max);
    }

    /** Next Post Link */
    if (get_next_posts_link())
        printf('<li class="next-post">%s</li>' . "\n", get_next_posts_link('<i class="fas fa-chevron-right"></i>'));

    echo '</ul></div>' . "\n";
}
