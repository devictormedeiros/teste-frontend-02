<?php include 'includes/cabecalho.php'; ?>

<body <?php body_class(); ?>>
	<header>
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="logo">
						<a href="<?php bloginfo('url') ?>" title="<?php bloginfo('name') ?>">
							<?php echo get_svg('logo'); ?>
						</a>
					</div>
				</div>
			</div>
		</div>
	</header>