<?php get_header(); ?>
<main id="main" class="site-main single_main" role="main">
	<section>
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-12 cabecalho-single">
					<h1><?php the_title(); ?></h1>
					<p class="resumo"><?php echo get_the_excerpt(); ?></p>
					<div class="toolbar">
						<?php include 'includes/data-info.php'; ?>
						<?php include 'includes/social-btns.php'; ?>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-9 pe-lg-5 post-content">
					<figure>
						<?php the_post_thumbnail(); ?>
					</figure>
					<?php the_content(); ?>
				</div>
				<div class="col-lg-3">
					<aside class="publi-container-single">
						<p><?php _e('Publicidade', 'seox-theme'); ?></p>
						<div class="publi-card">

						</div>
					</aside>
				</div>
			</div>
		</div>
	</section>
</main>



<?php get_footer(); ?>